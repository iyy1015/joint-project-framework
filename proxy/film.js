const film = {
  '/api': {
    target: 'https://api.iynn.cn/film',
    changeOrigin: true,
    // rewrite: path => path.replace(/^\/abc/, '')
  }
}

// 如果是自动导入,用commonjs模块化
module.exports = film
