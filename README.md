# 移动端电影
## 介绍
> 这是一个看最新电影应用
## 技术栈
>vue3 + pinia + vue-router + vite + ES6+ + axios + vant + 阿里图标iconfont + 其他
## 项目预览
> http://xxx.com/

## 依赖包版本
```
nodejs v16.14.2
├── @vitejs/plugin-vue-jsx@2.1.1
├── @vitejs/plugin-vue@3.2.0
├── axios@1.3.6
├── mockjs@1.1.0
├── pinia-plugin-persistedstate@3.1.0
├── pinia@2.0.34
├── sass@1.62.0
├── vite-plugin-mock@2.9.8
├── vite@3.2.6
├── vue-router@4.1.6
└── vue@3.2.47
```

