// 此模块化下面的数据是要在多个组件间共享
import { defineStore } from 'pinia'

// id,模块化的名称,唯一不能重复,相当于命名空间名称
// 一般情况下面和当前的文件名称一致
const useUserStore = defineStore('user', {
  persist: {
    key: 'user',
    storage: window.sessionStorage,
    paths: ['user']
  },
  // 数据 函数 返回一个对象
  state: () => ({
    user: {
      uid: 0,
      nickname: '默认昵称',
      token: ''
    }
  }),
  actions: {
    setUser(user) {
      // 单个字段的修改,可以用此方案
      this.user = user
    }
  }
})

export default useUserStore
