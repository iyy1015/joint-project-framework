// 是否已经登录,存储于pinia数据中
import useUserStore from '@/store/common'

export const checkLogin = () => (to, from) => {
  const userStore = useUserStore()
  if (to.meta.islogin) {
    if (!userStore.user.token) {
      return { path: '/login', replace: true }
    }
  }
}
