import { createRouter, createWebHistory } from 'vue-router'


import routes from './common'

// 防止翻墙
import * as hooks from './hooks'
// console.log(hooks);

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

Object.keys(hooks).forEach(key => {
  router.beforeEach(hooks[key]())
})

export default router
