import admin from './routes/admin'
import film from './routes/film'
// console.log(admin,film);
const routes = [
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: '用户登录页面'
    }
  },
  {
    path: '/',
    component: () => import('@/views/admin/index.vue'),
    meta: {
      title: '后台首页',
      islogin: true
    },
    children:[...admin,...film],
    // children2,
  },
  {
    path: '/:pathMatch(.*)',
    name: 'notfound',
    component: () => import('@/views/notfound/index.vue'),
    meta: {
      title: '404页面'
    }
  }
]

export default routes
