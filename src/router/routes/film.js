export default [
  {
    path: '/dashoard',
    component: () => import('@/views/dashoard/index.vue'),
    meta: {
      // 防止翻墙
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '页面汇总', url: '' }
      ]
    }
  },
  {
    path: '/film/list',
    component: () => import('@/views/filmlist/index.vue'),
    meta: {
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '电影设置', url: '' },
        { title: '电影用户列表', url: '' }
      ]
    }
  },
  {
    path: '/film/add',
    component: () => import('@/views/filmadd/index.vue'),
    meta: {
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '电影设置', url: '' },
        { title: '电影影片添加', url: '' }
      ]
    }
  },
]
