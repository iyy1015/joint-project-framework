export default [
  {
    path: '/dashoard',
    component: () => import('@/views/dashoard/index.vue'),
    meta: {
      // 防止翻墙
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '页面汇总', url: '' }
      ]
    }
  },
  {
    path: '/sys/user',
    component: () => import('@/views/user/index.vue'),
    meta: {
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '后台设置', url: '' },
        { title: '用户管理', url: '' }
      ]
    }
  },
  {
    path: '/sys/role',
    component: () => import('@/views/role/index.vue'),
    meta: {
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '后台设置', url: '' },
        { title: '角色管理', url: '' }
      ]
    }
  },
  {
    path: '/sys/auth',
    component: () => import('@/views/auth/index.vue'),
    meta: {
      islogin: true,
      crumb: [
        { title: '后台首页', url: '/dashoard' },
        { title: '后台设置', url: '' },
        { title: '权限管理', url: '' }
      ]
    }
  }
]
