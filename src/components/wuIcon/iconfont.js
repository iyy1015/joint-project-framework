;(window._iconfont_svg_string_3380251 =
  '<svg><symbol id="icon-yingyuan" viewBox="0 0 1024 1024"><path d="M449.6 851.1H352c-22.1 0-40 18-40 40s18 40 40 40h97.5c22.1 0 40-18 40-40s-17.8-40-39.9-40z m227.6 0h-97.6c-22.1 0-40 18-40 40s18 40 40 40h97.5c22.1 0 40-18 40-40s-17.9-40-39.9-40z m-455.2 0h-97.5c-22.1 0-40 18-40 40s18 40 40 40H222c22.1 0 40-18 40-40s-17.9-40-40-40zM976.7 92.8H47.3c-22.1 0-40 18-40 40v477.7c0 22.1 18 40 40 40h929.6c22.1 0 40-18 40-40V132.8c-0.1-22.1-18.1-40-40.2-40z m-40 477.6H87.2V172.9h849.5v397.5z m-37.2 280.7h-92.3c-22.1 0-40 18-40 40s18 40 40 40h92.3c22.1 0 40-18 40-40.1-0.1-21.9-18-39.9-40-39.9z m-117.2-99.2c0-22.1-17.9-40-39.9-40H281.6c-22.1 0-40 18-40 40s18 40 40 40h460.7c22.1 0 40-17.9 40-40z" ></path></symbol><symbol id="icon-yingyuan-active" viewBox="0 0 1024 1024"><path d="M449.6 851.1H352c-22.1 0-40 18-40 40s18 40 40 40h97.5c22.1 0 40-18 40-40s-17.8-40-39.9-40z m227.6 0h-97.6c-22.1 0-40 18-40 40s18 40 40 40h97.5c22.1 0 40-18 40-40s-17.9-40-39.9-40z m-455.2 0h-97.5c-22.1 0-40 18-40 40s18 40 40 40H222c22.1 0 40-18 40-40s-17.9-40-40-40zM976.7 92.8H47.3c-22.1 0-40 18-40 40v477.7c0 22.1 18 40 40 40h929.6c22.1 0 40-18 40-40V132.8c-0.1-22.1-18.1-40-40.2-40z m-40 477.6H87.2V172.9h849.5v397.5z m-37.2 280.7h-92.3c-22.1 0-40 18-40 40s18 40 40 40h92.3c22.1 0 40-18 40-40.1-0.1-21.9-18-39.9-40-39.9z m-117.2-99.2c0-22.1-17.9-40-39.9-40H281.6c-22.1 0-40 18-40 40s18 40 40 40h460.7c22.1 0 40-17.9 40-40z"></path></symbol></svg>'),
  (function (n) {
    var t = (t = document.getElementsByTagName('script'))[t.length - 1],
      e = t.getAttribute('data-injectcss'),
      t = t.getAttribute('data-disable-injectsvg')
    if (!t) {
      var c,
        i,
        o,
        s,
        d,
        a = function (t, e) {
          e.parentNode.insertBefore(t, e)
        }
      if (e && !n.__iconfont__svg__cssinject__) {
        n.__iconfont__svg__cssinject__ = !0
        try {
          document.write(
            '<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>'
          )
        } catch (t) {
          console && console.log(t)
        }
      }
      ;(c = function () {
        var t,
          e = document.createElement('div')
        ;(e.innerHTML = n._iconfont_svg_string_3380251),
          (e = e.getElementsByTagName('svg')[0]) &&
            (e.setAttribute('aria-hidden', 'true'),
            (e.style.position = 'absolute'),
            (e.style.width = 0),
            (e.style.height = 0),
            (e.style.overflow = 'hidden'),
            (e = e),
            (t = document.body).firstChild ? a(e, t.firstChild) : t.appendChild(e))
      }),
        document.addEventListener
          ? ~['complete', 'loaded', 'interactive'].indexOf(document.readyState)
            ? setTimeout(c, 0)
            : ((i = function () {
                document.removeEventListener('DOMContentLoaded', i, !1), c()
              }),
              document.addEventListener('DOMContentLoaded', i, !1))
          : document.attachEvent &&
            ((o = c),
            (s = n.document),
            (d = !1),
            h(),
            (s.onreadystatechange = function () {
              'complete' == s.readyState && ((s.onreadystatechange = null), l())
            }))
    }
    function l() {
      d || ((d = !0), o())
    }
    function h() {
      try {
        s.documentElement.doScroll('left')
      } catch (t) {
        return void setTimeout(h, 50)
      }
      l()
    }
  })(window)
