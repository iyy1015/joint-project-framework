import {post} from '@/utils/http'

/**
 * 用户登录接口
 * @param {object} userData {username:'',password:''}
 * @returns Promise
 * loginApi({username,password})
 */
export const loginApi = userData => post('/api/login',userData)
