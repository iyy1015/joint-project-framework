import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// 全局注册antd
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

// 注册全局组件
import globalComponent from './components'

// 引入pinia
import { createPinia } from 'pinia'
import persistedState from 'pinia-plugin-persistedstate'



const pinia = createPinia()
pinia.use(persistedState)

const app = createApp(App)

// 注册全局组件
globalComponent(app)

app.use(pinia)
app.use(router)

// 使用组件
app.use(Antd)

// app.use(Button)

app.mount('#app')
